@extends('layouts.app')
@section('content')
<div class="container mt-3">
    @if (!Auth::guest())
    @if(Auth::user()->id == 1)
    <p>Edit Artikel</p>
    {!! Form::open(['action' => ['BlogsController@update',$blog->id], 'method'=>'POST']) !!}
    <div class="form-group">
        {{Form::label('title', 'Title')}}
        {{Form::text('title', $blog->title,['class'=> 'form-control', 'placeholder'=>'Title'])}}
    </div>
    <div class="form-group">
        {{Form::label('body', 'Body')}}
        {{Form::textarea('body', $blog->body,['class'=> 'form-control', 'placeholder'=>'Body Text'])}}
    </div>
    {{Form::hidden('_method','PUT')}}
    {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
</div>
@endif
@endif
@endsection


