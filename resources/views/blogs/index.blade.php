@extends('layouts.app')
@section('content')

<img src='/img/image-title-blogs.png' class="img-navbar"/>

<div class="container">
    @if (!Auth::guest())
    @if(Auth::user()->id == 1)
    <div class="row row-blogs mt-5">
        <div class="col-8"></div>
        <div class="col-4">
            <a href="/artikel/create" class="btn btn-primary-tambah float-right">Tambah</a>
        </div>
    </div>
    @endif
    @endif
    @if (count($blogs) > 0)
            <div class="row card-deck row-card-deck">
                @foreach ($blogs as $blog)
                <div class="card mb-3 mt-3 ">
                    <div class="row no-gutters">
                      <div class="col-md-4">
                        @foreach ($imageBlogs as $imageBlog)
                        @if ($imageBlog['nama'] == $blog->cover_image)
                        <img src={{$imageBlog['image']}} class="card-img" alt="...">
                        @endif
                        @endforeach
                    </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title card-title-blog">{{$blog->title}}</h5>
                          <p class="card-text-blog line-clamp line-clamp-3">{{$blog->body}}</p>
                          <div class="col">
                            <a href="/artikel/{{$blog->id}}" type="button" class="btn btn-doctor btn-xs">Lihat Detail</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                @endforeach
            </div>
        </div>




    @else
    @endif
@endsection
