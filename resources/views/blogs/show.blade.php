@extends('layouts.app')
@section('content')
<div class="container mt-3">
    <h1>{{$blogs->title}}</h1>
    <p>{{$blogs->created_at_str}}</p>
    @if (count($imageBlogs) > 0)

    @foreach ($imageBlogs as $imageBlog)
    @if ($imageBlog['nama'] == $blogs->cover_image)
    <div class="row row-blogs mt-5 mb-5">
    <div class="col-12">
        <img class="img-blogs" src={{$imageBlog['image']}} alt="Card image cap">
    </div>
</div>
    @endif
    @endforeach
    @endif

    <div>
        {{$blogs->body}}
    </div>
</div>

    @if (!Auth::guest())
        @if(Auth::user()->id == 1)
        <div class="container mt-3">
            <div style="float: left">
                <a href="/artikel/{{$blogs->id}}/edit" class="btn btn-default">Edit</a>
            </div>
            <div style="float:right">
                {!!Form::open(['action' => ['BlogsController@destroy', $blogs->id], 'method' => 'POST'])!!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                {!!Form::close()!!}
            </div>
        </div>
                @endif
                @endif

@endsection
