@extends('layouts.app')
@section('content')
<img  src='/img/image-title-profile.png' class="img-navbar"/>

<div class="container mt-5">
<h2 class="mb-3">Tentang Kami</h2>
<img  src='/img/profile-header.jpeg' class="img-navbar"/>

<p class="mt-3">Bhayangkara Selapa Polri terletak di Jl. Ciputat Raya no. 40, Kebayoran Lama, Jakarta selatan. Dibangun diatas areal tanah seluas ± 100.000 M² dengan
    konsep desain bernuansa modern. RS Bhayangkara Selapa Polri sebelumnya adalah Poliklinik Selapa yang secara berangsur angsur dipersiapkan menjadi RS
    Bhayangkara Selapa di lingkungan Kepolisian Negara Republik Indonesia. RS Bhayangkara Selapa Polri memulai kegiatan operasional pada tahun 1980 dari
    sebuah poliklinik sekopol yang dipimpin oleh Kapten dr. tanti (Alm).</p>
<div class="container-fluid mt-5 mb-5">
	<div class="row">
		<div class="col-md-8 offset-md-2">
			<h4>Timeline Rumah Sakit</h4>
			<ul class="timeline">
				<li>
                    <p class="timeline-title">1980</p>
                    <p>Poliklinik Sekopol yang dipimpin oleh Kapten dr. Tanti ( Almh ) berubah nama menjadi Poliklinik Selapa Polri. Pada tahun 2006,
                        sesuai keputusan Kapolri No.Pol : Kep/I/II/2006</p>
                </li>
                <li>
                    <p class="timeline-title">9 Februari 2006</p>
                    <p>pembentukan RS Bhayangkara Tk IV di lingkungan Kepolisian Negara Republik Indonesia maka Poliklinik Selapa Polri ditetapkan
                        menjadi Rumah Sakit Bhayangkara Tk.IV Selapa Polri.
                    </p>
                </li>
                <li>
                    <p class="timeline-title">26 September 2014</p>
                    <p>maka secara legal mendapat ijin beroperasinya Rumah Sakit Bhayangkara Selapa Polri di Jakarta Selatan, yang pada saat itu di
                        pimpin oleh  dr. Maringan Simanjuntak, MM, MARS./p>
                </li>
                <li>
                    <p class="timeline-title">26 Juni 2007</p>
                    <p>Kaselapa Polri Brigadir Jenderal Polisi Drs. Budi Gunawan, S.H, Msi dibangun Rumah Sakit Bhayangkara Selapa Polri 2 lantai</p>
                </li>
                <li>
                    <p class="timeline-title">Oktober 2008</p>
                    <p>Pimpinan Rumah Sakit Bhayangkara Selapa Polri Kompol dr. Sumidi Sp.B menyerahkan tugasnya kepada Kompol drg. B. Dewanti, MM
                        dibawah pimpinan Kompol drg. B. Dewanti, MM</p>
                </li>
                <li>
                    <p class="timeline-title">3 November 2010</p>
                    <p>Rumah Sakit Bhayangkara Selapa Polri mendapat pengakuan telah memenuhi standar pelayanan rumah sakit yang meliputi: Administrasi
                        dan Manajemen, Pelayanan Medis, Pelayanan Gawat Darurat, Pelayanan Keperawatan, Rekam Medis dengan status AKREDITASI PENUH TINGKAT DASAR
                    </p>
                </li>
                <li>
                    <p class="timeline-title">18 Agustus 2011</p>
                    <p>Keputusan Kepala Sekolah Staf dan Pimpinan Pertama Polri memutuskan perubahan SOTK Rumah Sakit Bhayangkara Selapa Polri menjadi Rumah
                        Sakit Bhayangkara Sespimma Polri.</p>
                </li>
                <li>
                    <p class="timeline-title">Mei 2013 - November 2015</p>
                    <p>Rumah Sakit Bhayangkara Sespimma Polri dipimpin oleh kompol dr. Agus Pribadi,SpOG</p>
                </li>
                <li>
                    <p class="timeline-title">November 20165</p>
                    <p>Sampai dengan sekarang di pimpin oleh Pembina dr. Rini Afrianti, MKK, pada tanggal 14 oktober 2016 Rumah Sakit lulus ujian
                        akreditasi versi 2012 oleh KARS (Komite Akreditasi Rumah Sakit ) dengan lulus tingkat perdana.</p>

                    <p>Berdasarkan Surat Keputusan Kepala Dinas Kesehatan DKI Jakarta Nomor: 498 Tahun 2018 maka Rumah Sakit Bhayangkara Sespimma
                        Polri ditetapkan sebagai Rumah Sakit Umum Kelas C serta dikeluarkannya Surat Izin Operasional Rumah Sakit oleh Kepala Dinas
                         Penanaman Modal dan Pelayanan Terpadu Satu Pintu tentang Nomor : 36/B.3.7/31/-1.779.3/2018 dengan masa berlaku 9 Desember
                         2016 sampai dengan 9 Desember 2021</p>

                    <p>Berdasarkan Berita Acara Nomor : BA/ 05/ XII/ OTL.1.1.3./2018/SESPIMMA tentang Pengalihan dan Penyerahan Tanggung
                        Jawab Pembinaan Rumah Sakit Bhayangkara Sespimma Polri  dari Sespimma Sespim Lemdiklat Polri kepada Lemdiklat Polri
                        di karenakan berpindahnya Sespimma Sespim Lemdiklat Polri ke lembang bandung, maka Rumah Sakit Bhayangkara Sespimma
                        Polri secara administrasi berubah nama menjadi Rumah Sakit Bhayangkara Lemdiklat Polri.</p>
                </li>
                <li>
                    <p class="timeline-title">12 Agustus 2019</p>
                    <p>Rumah Sakit Bhayangkara Lemdiklat Polri telah lulus akreditasi tingkat paripurna ditetapkan oleh KARS (Komisi Akreditasi Rumah Sakit) , dasar hukum pelaksanaan akreditasi di rumah sakit adalah UU No.36 tahun 2009 tentang kesehatan, UU No.44 tahun 2009 tentsng rumah sakit dan permenkes 1144/Menkes/Per/VIII/2010 tentang organisasi dan tata kerja kementerian kesehatan.</p>
                </li>
			</ul>
		</div>
	</div>
</div>
</div>

@endsection
