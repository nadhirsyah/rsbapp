@extends('layouts.app')
@section('content')
<div class="container">
<div id="carouselExampleIndicators" class="carousel slide w-100" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="/img/seaside.jpg" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="/img/seaside.jpg" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="/img/seaside.jpg" alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
<div class='box-beranda'>
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card card-beranda rounded border-0 bg-transparent" style="width: 18rem;">
            <img class="card-img-top-beranda" src="/img/time.png" alt="Card image cap">
            <div class="card-body">
              <p class="card-title-beranda">Pelayanan 24 Jam</p>
               <p class="card-text-beranda">Pelayanan 7 hari dalam seminggu dengan dokter umum dan perawatan yang profesional didukung oleh dokter spesialis yang dapat dihubungi setiap waktu.</p>
            </div>
          </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card card-beranda rounded border-0 bg-transparent" style="width: 18rem;">
            <img class="card-img-top-beranda" src="/img/labo.png" alt="Card image cap">
            <div class="card-body">
              <p class="card-title-beranda">Laboratorium</p>
               <p class="card-text-beranda">Dilengkapi dengan peralatan mutakhir dan didukung oleh staf yang terdiri dari para dokter dan Analis yang professional dan sangat berpengalaman.</p>
            </div>
          </div>    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card card-beranda rounded border-0 bg-transparent" style="width: 18rem;">
            <img class="card-img-top-beranda" src="/img/medic.png" alt="Card image cap">
            <div class="card-body">
              <p class="card-title-beranda">Tenaga Medis</p>
               <p class="card-text-beranda">Kami didukung oleh tenaga kerja medis yang terlatih dan memiliki sertifikat penanggulangan pasien Gawat Darurat (PPGD). Advance Cardiac Life Support (ACLS), Advance Trauma Life Support (ATLS).</p>
            </div>
          </div>    </div>
    <div class="col-lg-3 col-md-6 col-sm-12">
        <div class="card card-beranda rounded border-0 bg-transparent" style="width: 18rem;">
            <img class="card-img-top-beranda" src="/img/ambulance.png" alt="Card image cap">
            <div class="card-body">
              <p class="card-title-beranda">Unit UGD</p>
               <p class="card-text-beranda">Instalasi Gawat Darurat RS. BHAYANGKARA LEMDIKLAT POLRI melayani kegawatdaruratan medik baik kasus trauma maupun non trauma, termasuk resusitasi secara optimal dan profesional.</p>
            </div>
          </div>
        </div>
  </div>
</div>
<div class="container mt-4">
    <div id="textbox">
        <p class="alignleft"><strong>Dokter</strong> kami</p>
        <p class="alignright">Lihat Semua</p>
      </div>
      <div style="clear: both;"></div>
<hr class="style-six"/>
<div class="owl-carousel-services owl-carousel owl-theme mt-1">
    @foreach ($services as $service)
    <div class="item card rounded shadow-sm card-home-services">
        <img class="card-img-top" src='/img/cover_images/{{$service->cover_image}}' alt="Card image cap">
        <div class="card-body">
          <p class="card-title-blog">{{$service->title}}</p>
          <p class="card-text-blog">{{$service->body}}</p>
        </div>
      </div>
      @endforeach
  </div>
</div>
<div class="container mt-4">
    <div id="textbox">
        <p class="alignleft"><strong>Layanan</strong> kami</p>
        <p class="alignright">Lihat Semua</p>
      </div>
      <div style="clear: both;"></div>
<hr class="style-six"/>
  <div class="owl-carousel-doctor owl-carousel owl-theme mt-5">
    @foreach ($doctors as $doctor)
    <div class="item card rounded shadow-sm card-home-services">
        <img class="card-img-top" src='/img/cover_images/{{$doctor->cover_image}}' alt="Card image cap">
        <div class="card-body">
          <p class="card-title-blog">{{$doctor->name}}</p>
          <p class="card-text-blog">{{$doctor->spesialis}}</p>
        </div>
      </div>
      @endforeach
  </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-7">
            <div id="textbox">
                <p class="alignleft"><strong>Layanan</strong> kami</p>
            </div>
            <div style="clear: both;"></div>
            <hr class="style-six"/>

            @foreach ($blogs as $blog)
            <div class="card border-0 mb-2">
                <div class="card-horizontal">
                    <div class="img-square-wrapper">
                        <img class="rounded-img" src='/img/cover_images/{{$blog->cover_image}}' alt="Card image cap">
                    </div>
                <div class="card-body card-body-blog">
                    <h4 class="card-title card-title-blog">{{$blog->title}}</h4>
                    <p class="card-text card-text-blog overflow">{{$blog->body}}.</p>
                </div>
            </div>
        </div>
    @endforeach
</div>
    <div class="col-5">
        <div id="textbox">
            <p class="alignleft"><strong>Kamar</strong> tersedia</p>
        </div>
        <div style="clear: both;"></div>
        <hr class="style-six"/>
        <table class="zui-table zui-table-rounded">
            <tbody>

                 @foreach ($rooms as $room)

                <tr>
                    <td>{{$room['nama']}}</td>
                    <td>{{$room['jumlah']}}</td>
                    <td>{{$room['isi']}}</td>
                    <td>{{$room['kosong']}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </div>


</div>
@endsection
