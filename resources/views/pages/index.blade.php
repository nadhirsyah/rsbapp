@extends('layouts.app')
@section('content')
<div class="container-fluid container-beranda">
    <div id="carouselExampleIndicators" class="carousel slide w-100" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src='/img/profile-header.jpeg' alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src='/img/profile-header.jpeg' alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src='/img/profile-header.jpeg' alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
</div>
<div class="container-fluid container-beranda">
    <div class='box-beranda'>
        <div class="row row-box-beranda">
        <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="card card-box-beranda bg-transparent">
                <img class="card-img-top-beranda" src="/img/time.png" alt="Card image cap">
                <div class="card-body card-body-beranda">
                    <p class="card-title-beranda">Pelayanan 24 Jam</p>
                     <p class="card-text-beranda">Pelayanan 7 hari dalam seminggu dengan dokter umum dan perawatan yang profesional didukung oleh dokter spesialis yang dapat dihubungi setiap waktu.</p>
                  </div>
              </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="card card-box-beranda bg-transparent" >
                <img class="card-img-top-beranda" src="/img/labo.png" alt="Card image cap">
                <div class="card-body card-body-beranda">
                    <p class="card-title-beranda">Laboratorium</p>
                     <p class="card-text-beranda">Dilengkapi dengan peralatan mutakhir dan didukung oleh staf yang terdiri dari para dokter dan Analis yang professional dan sangat berpengalaman.</p>
                  </div>
              </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="card card-box-beranda bg-transparent" >
                <img class="card-img-top-beranda" src="/img/medic.png" alt="Card image cap">
                <div class="card-body card-body-beranda">
                    <p class="card-title-beranda">Tenaga Medis</p>
                     <p class="card-text-beranda">Kami didukung oleh tenaga kerja medis yang terlatih dan memiliki sertifikat penanggulangan pasien Gawat Darurat (PPGD). Advance Cardiac Life Support (ACLS), Advance Trauma Life Support (ATLS).</p>
                  </div>
              </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="card card-box-beranda bg-transparent" >
                <img class="card-img-top-beranda" src="/img/ambulance.png" alt="Card image cap">
                <div class="card-body card-body-beranda">
                    <p class="card-title-beranda">Unit UGD</p>
                     <p class="card-text-beranda">Instalasi Gawat Darurat RS. BHAYANGKARA LEMDIKLAT POLRI melayani kegawatdaruratan medik baik kasus trauma maupun non trauma, termasuk resusitasi secara optimal dan profesional.</p>
                  </div>
              </div>
        </div>
    </div>
    </div>
    <div class="container mt-4">
        <div class="container-fluid container-beranda">
            <div class="row row-card-beranda">
                <div class="col-6">
                    <p class="alignleft"><strong>Layanan</strong> kami</p>
                </div>
                <div class="col-6">
                    <a href="/layanan"><p class="alignright">Lihat Semua</p></a>
                </div>
            </div>
        </div>
    <hr class="style-six"/>
    @if (count($services) > 0)
    <div class="owl-carousel-services owl-carousel owl-theme mt-1">
        @foreach ($services as $service)
        @if (count($imageServices) > 0)
        <div class="item card rounded shadow-sm card-home-services">
            @foreach ($imageServices as $imageService)
            @if ($imageService['nama'] == $service->cover_image)
            <div class="block-card-services">
                <img class="card-img-top card-img-top-services" src={{$imageService['image']}} alt="Card image cap" >
            </div>
            @endif
            @endforeach
            <div class="card-body">
              <p class="card-title-blog">{{$service->title}}</p>
              <p class="card-text-blog line-clamp line-clamp-2">{{$service->body}}</p>
            </div>
          </div>
          @endif
          @endforeach
      </div>
    </div>
    @endif
    <div class="container mt-4">
        <div class="container-fluid container-beranda">
            <div class="row row-card-beranda">
                <div class="col-6">
                    <p class="alignleft"><strong>Dokter</strong> kami</p>
                </div>
                <div class="col-6">
                    <a href="/dokters"><p class="alignright">Lihat Semua</p></a>
                </div>
            </div>
        </div>
    <hr class="style-six"/>
    @if (count($doctors) > 0)
    <div class="owl-carousel-services owl-carousel owl-theme mt-1">
        @foreach ($doctors as $doctor)
        @if (count($imageDoctors) > 0)
        <div class="item card rounded shadow-sm card-home-services">
            @foreach ($imageDoctors as $imageDoctor)
            @if ($imageDoctor['nama'] == $doctor->cover_image)
            <div class="block-card-services">
                <img class="card-img-top card-img-top-services" src={{$imageDoctor['image']}} alt="Card image cap" >
            </div>
            @endif
            @endforeach
            <div class="card-body">
                <p class="card-title-blog">{{$doctor->name}}</p>
                <p class="card-text-blog">{{$doctor->spesialis}}</p>
            </div>
          </div>
          @endif
          @endforeach
      </div>
      @endif
    </div>
</div>
<div class="container-fluid">
    <div class="row row-box-beranda">
        <div class="col-lg-7 col-md-12">
            <div class="container-fluid container-beranda">
                <div class="row row-card-beranda">
                    <div class="col-6">
                        <p class="alignleft"><strong>Berita</strong> kami</p>
                    </div>
                    <div class="col-6">
                        <a href="/artikel"><p class="alignright-berita">Lihat Semua</p></a>
                    </div>
                </div>
            </div>
            <hr class="style-six"/>
            @if (count($blogs) > 0)
            @foreach ($blogs as $blog)
                <div class="card mb-3 mt-3 card-artikel">
                    <div class="row no-gutters">
                      <div class="col-md-4">
                        @foreach ($imageBlogs as $imageBlog)
                        @if ($imageBlog['nama'] == $blog->cover_image)
                        <img src={{$imageBlog['image']}} class="card-img" alt="...">
                        @endif
                        @endforeach
                    </div>
                      <div class="col-md-8">
                        <div class="card-body">
                          <h5 class="card-title card-title-blog">{{$blog->title}}</h5>
                          <p class="card-text-blog line-clamp line-clamp-3">{{$blog->body}}</p>
                          <div class="col">
                            <a href="/artikel/{{$blog->id}}" type="button" class="btn btn-doctor btn-xs">Lihat Detail</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                @endforeach
            @endif
        </div>

    <div class="col-lg-5 col-md-12">
        <div class="container-fluid container-beranda">
            <div class="row row-card-beranda">
                    <p class="alignleft"><strong>Kamar</strong> tersedia</p>
            </div>
        </div>
        <div style="clear: both;"></div>
        <hr class="style-six"/>
        <table class="data">
            <tr>
              <th>Nama Kamar</th>
              <th>Jumlah Tersedia</th>
            </tr>
            @foreach ($rooms as $room)
            <tr>
              <td class="table-name">{{$room['nama']}}</td>
              <td class="table-value">{{$room['kosong']}}</td>
            </tr>
            @endforeach

          </table>
    </div>
    </div>


</div>
<footer>
    <div class="container">
    </div>
    <div class="copyright text-center">
     &copy; <span>RS BHAYANGKARA LEMDIKLAT 2020</span>
    </div>
  </footer>

@endsection
