@extends('layouts.app')
@section('content')
<img src='/img/image-title-services.png' class="img-navbar"/>

<div class="container services">
    @if (!Auth::guest())
    @if(Auth::user()->id == 1)
    <div class="row row-blogs mt-5">
        <div class="col-8"></div>
        <div class="col-4">
            <a href="/layanan/create" class="btn btn-primary-tambah float-right">Tambah</a>
        </div>
    </div>
    @endif
    @endif
    @if (count($services) > 0)

    <div class="row card-deck display-flex">
        @foreach ($services as $service)
            <div class="col-lg-4 col-sm-8 col-md-6 py-3 px-2 col-services">
                @if (count($imageServices) > 0)

            <div class="card rounded shadow-sm card-services">
            @foreach ($imageServices as $imageService)
            @if ($imageService['nama'] == $service->cover_image)
            <div class="card-img-services">
                <img class="card-img-top img-services" src={{$imageService['image']}} alt="Card image cap">
            </div>
            @endif
            @endforeach
            @endif


                <div class="card-body card-body-services">
                  <p class="card-title-blog">{{$service->title}}</p>
                  <p class="card-text-blog line-clamp line-clamp-2">{{$service->body}}</p>
                  <div class="row row-card-deck">
                    <div class="col text-center">
                      <a href="/layanan/{{$service->id}}" type="button" class="btn btn-doctor btn-xs">Lihat Detail</a>
                    </div>
                </div>
                </div>
              </div>
            </div>
        @endforeach
    </div>
    @endif
</div>
@endsection
