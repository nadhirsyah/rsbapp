@extends('layouts.app')
@section('content')
<div class="container mt-3">
    @if (!Auth::guest())
    @if(Auth::user()->id == 1)
    <p>Tambah Layanan</p>
    {!! Form::open(['action' => 'ServicesController@store', 'method'=>'POST', 'enctype'=> 'multipart/form-data']) !!}
   <div class="form-group">
       {{Form::label('title', 'Title')}}
       {{Form::text('title', '',['class'=> 'form-control', 'placeholder'=>'Title'])}}
   </div>
   <div class="form-group">
    {{Form::label('body', 'Body')}}
    {{Form::textarea('body', '',['class'=> 'form-control', 'placeholder'=>'Body Text'])}}
    </div>
    <div class="form-group">
        {{Form::label('cover_image', 'Cover Image')}}
        {{Form::file('cover_image',['class'=> 'form-control form-file'])}}
    </div>
    {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
</div>
@endif
@endif
@endsection

