@extends('layouts.app')
@section('content')
<div class="container mt-3">
    @if (!Auth::guest())
    @if(Auth::user()->id == 1)
    <p>Edit Layanan</p>
   {!! Form::open(['action' => ['ServicesController@update',$service->id], 'method'=>'POST']) !!}
   <div class="form-group">
       {{Form::label('title', 'Title')}}
       {{Form::text('title', $service->title,['class'=> 'form-control', 'placeholder'=>'Title'])}}
   </div>
   <div class="form-group">
    {{Form::label('body', 'Body')}}
    {{Form::textarea('body', $service->body,['class'=> 'form-control', 'placeholder'=>'Body Text'])}}
</div>
{{Form::hidden('_method','PUT')}}
{{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
{!! Form::close() !!}
</div>
@endif
@endif
@endsection

