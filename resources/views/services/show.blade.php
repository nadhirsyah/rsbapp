@extends('layouts.app')
@section('content')
@if ($service)

<div class="container mt-3">
    <h1>{{$service->title}}</h1>
    @if (count($imageServices) > 0)
    @foreach ($imageServices as $imageService)
    @if ($imageService['nama'] == $service->cover_image)
    <img class="img-doctors mb-3 mt-3" src={{$imageService['image']}} alt="Card image cap">
    @endif
    @endforeach
    @endif

<br>
<br>
    <div class="mb-5">
        {{$service->body}}
    </div>
    @endif
    @if (!Auth::guest())
    @if(Auth::user()->id == 1)
    <div class="container mt-3">
        <div style="float: left">
            <a href="/layanan/{{$service->id}}/edit" class="btn btn-default">Edit</a>
        </div>
        <div style="float:right">
    {!!Form::open(['action' => ['ServicesController@destroy', $service->id], 'method' => 'POST'])!!}
            {{Form::hidden('_method', 'DELETE')}}
            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
            {!!Form::close()!!}
        </div>
        @endif
        @endif

@endsection
