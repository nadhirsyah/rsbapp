

@extends('layouts.app')
@section('content')
<div class="container mt-5">
<table class="data">
    <tr>
      <th>Nama Kamar</th>
      <th class="table-value-beds">Jumlah Bed</th>
      <th class="table-value-beds">Bed Terisi</th>
      <th class="table-value-beds">Bed Kosong</th>
    </tr>
    @foreach ($data as $room)
    <tr>
      <td class="table-name">{{$room['nama']}}</td>
      <td class="table-value-beds">{{$room['jumlah']}}</td>
      <td class="table-value-beds">{{$room['isi']}}</td>
      <td class="table-value-beds">{{$room['kosong']}}</td>
    </tr>
    @endforeach

  </table>
</div>
@endsection
