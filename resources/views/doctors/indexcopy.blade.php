@extends('layouts.app')
@section('content')
<div class="row row-margin">
    <div class="col-md-3 sidebar-col">
<div class="sidebar">
    <a class="active" href="#home">Poli Spesialis Bedah Umum</a>
    <a href="#news">News</a>
    <a href="#contact">Contact</a>
    <a href="#about">About</a>
  </div>


    </div>
    <div class="col-md-9 content">
  @if (count($doctors) > 0)
<div class="row card-deck row-card-deck">
            @foreach ($doctors as $doctor)
            <div class="col-xl-4 col-md-6 card-col">
                <div class="card rounded shadow-sm card-doctor">
                    <img class="card-img-top card-img-top-doctor" src='/img/cover_images/{{$doctor->cover_image}}' alt="Card image cap">
                    <div class="card-body card-body-doctor">
                      <p class="card-title-doctor">{{$doctor->name}}</p>
                      <p class="card-text-doctor">{{$doctor->spesialis}}</p>
                    </div>
                  </div>
            </div>
            @endforeach
        </div>
</div>
</div>

    @else
        <p>no posts found</p>
    @endif
@endsection
