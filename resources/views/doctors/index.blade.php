@extends('layouts.app')
@section('content')

<img src='/img/image-title-doctors.png' class="img-navbar"/>
@if (!Auth::guest())
@if(Auth::user()->id == 1)
<div class="container">
<div class="row row-blogs mt-5">
    <div class="col-8"></div>
    <div class="col-4">
        <a href="/dokters/create" class="btn btn-primary-tambah float-right">Tambah</a>
    </div>
</div>
</div>
@endif
@endif
<div class="row row-margin">
    <div class="col-md-4 col-sm-12 col-margin-doctor">
        <div class="sidebar">
        <a href="/dokter/hemodialisa">Hemodialisa</a>
        <a href="/dokter/igd">Instalasi Gawat Darurat</a>
        <a href="/dokter/gigi">Klinik Gigi</a>
        <a href="/dokter/kecantikan">Klinik Kecantikan & Estetika</a>
        <a href="/dokter/anak">Klinik Spesialis Anak</a>
        <a href="/dokter/tht">Klinik Spesialis THT</a>
        <a href="/dokter/bedah-umum">Poli Spesialis Bedah Umum</a>
        <a href="/dokter/saraf">Spesialis Saraf</a>
        <a href="/dokter/jiwa">Spesialis Jiwa</a>
        <a href="/dokter/kandungan">Spesialis Kandungan</a>
        <a href="/dokter/paru">Spesialis Paru</a>
        <a href="/dokter/dalam">Spesialis Penyakit Dalam</a>
        <a href="/dokter/radiologi">Unit Radiologi</a>
      </div>
    </div>
    <div class="col-md-8 col-sm-12 col-content-margin-doctor">

@if (count($doctors) > 0)

<div class="row equal card-deck row-blogs display-flex">
            @foreach ($doctors as $doctor)
            <div class="col-md-4 col-sm-6 card-col">
                <div class="card rounded shadow-sm card-doctor ">
                    @if (count($imageDoctors) > 0)

                    @foreach ($imageDoctors as $imageDoctor)
            @if ($imageDoctor['nama'] == $doctor->cover_image)
            <img class="card-img-top card-img-top-doctor"  src={{$imageDoctor['image']}} alt="Card image cap">
            @endif
            @endforeach
            @endif
                    <div class="card-body card-body-doctor">
                      <p class="card-title-doctor">{{$doctor->name}}</p>
                      <p class="card-text-doctor">{{$doctor->spesialis}}</p>
                      <div class="row row-card-deck">
                          <div class="col text-center">
                            <a href="/dokters/{{$doctor->id}}" type="button" class="btn btn-doctor btn-xs">Lihat Detail</a>
                          </div>
                      </div>
                    </div>
                  </div>
            </div>
            @endforeach
        </div>
    @endif

    <!-- /bootstrap grid example -->
</div>
</div>
@endsection
