@extends('layouts.app')
@section('content')
<div class="container mt-3">
    <div class="row">
        <div class="col-4">
            @if (count($imageDoctors) > 0)

            @foreach ($imageDoctors as $imageDoctor)
            @if ($imageDoctor['nama'] == $doctor->cover_image)
            <img class="img-doctors"src={{$imageDoctor['image']}} alt="Card image cap">
            @endif
            @endforeach
            @endif

        </div>
        <div class="col-8">
            <h1>{{$doctor->name}}</h1>
            <div>
                {{$doctor->spesialis}}
            </div>
            {{-- @if ($doctor->sabtu_mulai && !$doctor->sabtu_selesai)
            <h1>{{$doctor->sabtu_mulai}}</h1>
            @endif --}}
            @if ($doctor->senin_mulai || $doctor->senin_selesai || $doctor->selasa_mulai || $doctor->selasa_selesai || $doctor->rabu_mulai || $doctor->rabu_selesai || $doctor->kamis_mulai || $doctor->kamis_selesai || $doctor->jumat_mulai || $doctor->jumat_selesai || $doctor->sabtu_mulai || $doctor->sabtu_selesai)
            <table class="data">
                <tr>
                  <th>Hari</th>
                  <th class="table-value">Jam Prakter</th>
                </tr>
                @if ($doctor->senin_mulai && $doctor->senin_selesai)
                <tr>
                    <td class="table-name">Senin</td>
                    <td class="table-value">{{$doctor->senin_mulai}}-{{$doctor->senin_selesai}}</td>
                  </tr>
                @endif
                @if ($doctor->selasa_mulai && $doctor->selasa_selesai)
                <tr>
                    <td class="table-name">Selasa</td>
                    <td class="table-value">{{$doctor->selasa_mulai}}-{{$doctor->selasa_selesai }}</td>
                  </tr>
                @endif
                @if ($doctor->rabu_mulai && $doctor->rabu_selesai)
                <tr>
                    <td class="table-name">Rabu</td>
                    <td class="table-value">{{$doctor->rabu_mulai}}-{{$doctor->rabu_selesai }}</td>
                  </tr>
                @endif
                @if ($doctor->kamis_mulai && $doctor->kamis_selesai)
                <tr>
                    <td class="table-name">Kamis</td>
                    <td class="table-value">{{ $doctor->kamis_mulai }}-{{$doctor->kamis_selesai}}</td>
                  </tr>
                @endif
                @if ($doctor->jumat_mulai && $doctor->jumat_selesai)
                <tr>
                    <td class="table-name">Jumat</td>
                    <td class="table-value">{{$doctor->jumat_mulai }}-{{$doctor->jumat_selesai }}</td>
                  </tr>
                @endif
                @if ($doctor->sabtu_mulai && $doctor->sabtu_selesai)
                <tr>
                    <td class="table-name">Sabtu</td>
                    <td class="table-value">{{$doctor->sabtu_mulai }}-{{$doctor->sabtu_selesai }}</td>
                  </tr>
                @endif
                @if ($doctor->senin_mulai && !$doctor->senin_selesai)
                <tr>
                    <td class="table-name">Senin</td>
                    <td class="table-value">{{$doctor->senin_mulai}}-Selesai</td>
                  </tr>
                @endif
                @if ($doctor->selasa_mulai && !$doctor->selasa_selesai)
                <tr>
                    <td class="table-name">Selasa</td>
                    <td class="table-value">{{$doctor->selasa_mulai}}-Selesai</td>
                  </tr>
                @endif
                @if ($doctor->rabu_mulai && !$doctor->rabu_selesai)
                <tr>
                    <td class="table-name">Rabu</td>
                    <td class="table-value">{{$doctor->rabu_mulai}}-Selesai</td>
                  </tr>
                @endif
                @if ($doctor->kamis_mulai && !$doctor->kamis_selesai)
                <tr>
                    <td class="table-name">Kamis</td>
                    <td class="table-value">{{ $doctor->kamis_mulai }}-Selesai</td>
                  </tr>
                @endif
                @if ($doctor->jumat_mulai && !$doctor->jumat_selesai)
                <tr>
                    <td class="table-name">Jumat</td>
                    <td class="table-value">{{$doctor->jumat_mulai }}-Selesai</td>
                  </tr>
                @endif
                @if ($doctor->sabtu_mulai && !$doctor->sabtu_selesai)
                <tr>
                    <td class="table-name">Sabtu</td>
                    <td class="table-value">{{$doctor->sabtu_mulai }}-Selesai</td>
                  </tr>
                @endif
              </table>
              @endif
        </div>
    </div>
</div>
@if (!Auth::guest())
@if(Auth::user()->id == 1)
{{-- <div class="container mx-3"> --}}
    <div class="container mt-3">
    <div style="float: left">
    <a href="/dokters/{{$doctor->id}}/edit" class="btn btn-default">Edit</a>
</div>
<div style="float:right">
    {!!Form::open(['action' => ['DoctorsController@destroy', $doctor->id], 'method' => 'POST'])!!}
            {{Form::hidden('_method', 'DELETE')}}
            {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
            {!!Form::close()!!}
        </div>
        </div>
            @endif
            @endif


@endsection
