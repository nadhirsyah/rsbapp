@extends('layouts.app')
@section('content')
<div class="container mt-3">
    @if (!Auth::guest())
    @if(Auth::user()->id == 1)

   <p>Tambah Doctor</p>
   {!! Form::open(['action' => 'DoctorsController@store', 'method'=>'POST', 'enctype'=> 'multipart/form-data']) !!}
   <div class="form-group">
       {{Form::label('name', 'Name')}}
       {{Form::text('name', '',['class'=> 'form-control', 'placeholder'=>'Name'])}}
   </div>
   <div class="form-group">
    {{Form::label('spesialis', 'Spesialis')}}
    {{Form::text('spesialis', '',['class'=> 'form-control', 'placeholder'=>'Spesialis'])}}
</div>
<div class="form-group">
    {{Form::label('cover_image', 'Cover Image')}}
    {{Form::file('cover_image',['class'=> 'form-control form-file'])}}
</div>
<div class="form-group">
    {{ Form::label('senin_mulai', 'Senin Mulai:') }}
    {{ Form::time('senin_mulai', '') }}

    {{ Form::label('senin_selesai', 'Senin Selesai:') }}
    {{ Form::time('senin_selesai',  '') }}
</div>
<div class="form-group">
    {{ Form::label('selasa_mulai', 'Selasa Mulai:') }}
    {{ Form::time('selasa_mulai', '') }}

    {{ Form::label('selasa_selesai', 'Selasa Selesai:') }}
    {{ Form::time('selasa_selesai',  '') }}
</div>

<div class="form-group">
    {{ Form::label('rabu_mulai', 'Rabu Mulai:') }}
    {{ Form::time('rabu_mulai',  '') }}

    {{ Form::label('rabu_selesai', 'Rabu Selesai:') }}
    {{ Form::time('rabu_selesai',  '') }}
</div>

<div class="form-group">
    {{ Form::label('kamis_mulai', 'Kamis Mulai:') }}
    {{ Form::time('kamis_mulai',  '') }}

    {{ Form::label('kamis_selesai', 'Kamis Selesai:') }}
    {{ Form::time('kamis_selesai',  '') }}
</div>

<div class="form-group">
    {{ Form::label('jumat_mulai', 'Jumat Mulai:') }}

    {{ Form::time('jumat_mulai', '') }}

    {{ Form::label('jumat_selesai', 'Jumat Selesai:') }}
    {{ Form::time('jumat_selesai', '') }}
</div>

<div class="form-group">
    {{ Form::label('sabtu_mulai', 'Sabtu Mulai:') }}

    {{ Form::time('sabtu_mulai', '') }}

    {{ Form::label('sabtu_selesai', 'Sabtu Selesai:') }}
    {{ Form::time('sabtu_selesai', '') }}
</div>

{{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
{!! Form::close() !!}
</div>
@endif
@endif
@endsection

