<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->mediumText('body');
            $table->time('senin_mulai')->nullable();
            $table->time('senin_selesai')->nullable();

            $table->time('selasa_mulai')->nullable();
            $table->time('selasa_selesai')->nullable();

            $table->time('rabu_mulai')->nullable();
            $table->time('rabu_selesai')->nullable();

            $table->time('kamis_mulai')->nullable();
            $table->time('kamis_selesai')->nullable();

            $table->time('jumat_mulai')->nullable();
            $table->time('jumat_selesai')->nullable();

            $table->time('sabtu_mulai')->nullable();
            $table->time('sabtu_selesai')->nullable();

            $table->time('minggu_mulai')->nullable();
            $table->time('minggu_selesai')->nullable();
            $table->string('cover_image');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
