<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Blog;

use App\Doctor;

use App\Service;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Log;

use JD\Cloudder\Facades\Cloudder;


class PagesController extends Controller
{
    public function generateRoom($data)
    {
        $dataConvert = [];
        $dataInitial = $data[0]->kelas;
        $count = 0;
        $countIsi = 0;
        $countKosong = 0;
        $index = 0;
        foreach ($data as $this_data) {

            $this_data_kelas = $this_data->kelas;
            $this_data_status = $this_data->status;



            if ($this_data_kelas != $dataInitial) {

                $dataConvert[] = ['nama' => $dataInitial, 'jumlah' => $count, 'isi' => $countIsi, 'kosong' => $countKosong];
                $dataInitial = $this_data->kelas;
                $count = 0;
                $countIsi = 0;
                $countKosong = 0;
            }
            $count += 1;
            if ($this_data_status == 'ISI') {
                $countIsi += 1;
            } else if ($this_data_status == 'KOSONG') {
                $countKosong += 1;
            }

            if (count($data) - 1 == $index) {
                $dataConvert[] = ['nama' => $dataInitial, 'jumlah' => $count, 'isi' => $countIsi, 'kosong' => $countKosong];
            }
            $index += 1;
        }

        return $dataConvert;
    }
    public function index()
    {
        $blogs = Blog::orderBy('created_at', 'desc')->paginate(10);
        $services = Service::orderBy('created_at', 'desc')->paginate(10);
        $doctors = Doctor::orderBy('created_at', 'desc')->paginate(10);
        $room = DB::connection('mysql2')->select('Select kelas,status,kd_kamar from kamar where statusdata=2 ORDER BY KELAS');
        $rooms = $this->generateRoom($room);
        $imageServices = [];
        $imageDoctors = [];
        $imageBlogs = [];


        foreach ($services as $service) {
            if (Cloudder::show($service->cover_image)) {

                $image = Cloudder::show($service->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_320,q_100,w_320');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageServices[] = ['nama' => $service->cover_image, 'image' => $comma_separated];
            }
        }
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_320,q_100,w_320');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        foreach ($blogs as $blog) {
            if (Cloudder::show($blog->cover_image)) {

                $image = Cloudder::show($blog->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_320,q_100,w_320');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageBlogs[] = ['nama' => $blog->cover_image, 'image' => $comma_separated];
            }
        }


        $data = array('blogs' => $blogs, 'services' => $services, 'doctors' => $doctors, 'rooms' => $rooms, 'imageServices' => $imageServices, 'imageDoctors' => $imageDoctors, 'imageBlogs' => $imageBlogs);
        return view('pages.index')->with($data);
    }
    public function about()
    {
        return view('pages.about');
    }
}
