<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use JD\Cloudder\Facades\Cloudder;

use Illuminate\Support\Facades\Log;

class ImageUploadController extends Controller
{
    public function home()
    {
        return view('upload');
    }
    public function uploadImages(Request $request)
    {
        $this->validate($request, [
            'image_name' => 'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
        ]);
        $filenameWithExt = $request->file('image_name')->getClientOriginalName();

        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

        $image_name = $request->file('image_name')->getRealPath();;

        Cloudder::upload($image_name, $filename);

        return redirect()->back()->with('status', 'Image Uploaded Successfully');
    }
    public function show()
    {
        $image = Cloudder::show("84klnik_anak_1596682257");
        return view('show', ['image' => $image]);
    }
}
