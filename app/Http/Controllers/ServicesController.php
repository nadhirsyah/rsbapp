<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Service;

use Illuminate\Support\Facades\Log;

use JD\Cloudder\Facades\Cloudder;

class ServicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $services = Service::orderBy('title', 'asc')->get();
        $services = Service::orderBy('created_at', 'desc')->get();
        $imageServices = [];

        foreach ($services as $service) {
            if (Cloudder::show($service->cover_image)) {

                $image = Cloudder::show($service->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    }
                }
                $comma_separated = implode("/", $array);
                $imageServices[] = ['nama' => $service->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('services' => $services, 'imageServices' => $imageServices);

        return view('services.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'nullable|mimes:jpeg,bmp,jpg,png|between:1, 6000',
        ]);

        if ($request->hasFile('cover_image')) {
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $image_name = $request->file('cover_image')->getRealPath();;

            $fileNameToStore = $filename . '_' . time();

            Cloudder::upload($image_name, $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $service = new Service;
        $service->title = $request->input('title');
        $service->body = $request->input('body');
        $service->cover_image = $fileNameToStore;

        $service->save();
        return redirect('/layanan')->with('success', 'Layanan Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::find($id);
        $imageServices = [];
        Log::emergency($service->cover_image);

        if (Cloudder::show($service->cover_image)) {

            $image = Cloudder::show($service->cover_image);

            $var = preg_split("#/#", $image);
            $array = [];
            for ($x = 0; $x < count($var); $x++) {
                if ($x != 6) {
                    array_push($array, $var[$x]);
                }
            }
            $comma_separated = implode("/", $array);
            $imageServices[] = ['nama' => $service->cover_image, 'image' => $comma_separated];
        }
        $data = array('service' => $service, 'imageServices' => $imageServices);

        return view('services.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::find($id);
        return view('services.edit')->with('service', $service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
        ]);

        $service = Service::find($id);
        $service->title = $request->input('title');
        $service->body = $request->input('body');
        $service->save();
        return redirect('/layanan')->with('success', 'Layanan Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);
        $service->delete();
        return redirect('/layanan')->with('success', 'Layanan Dihapus');
    }
}
