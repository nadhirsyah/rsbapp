<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Doctor;

use Carbon\Carbon;

use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Storage;


use JD\Cloudder\Facades\Cloudder;

class DoctorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show', 'index_hemodialisa', 'index_igd', 'index_gigi', 'index_kecantikan', 'index_bedah_umum', 'index_anak', 'index_jiwa', 'index_kandungan', 'index_dalam', 'index_saraf', 'index_radiologi', 'index_tht', 'index_paru']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctors = Doctor::orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }

    public function index_hemodialisa()
    {
        $doctors = Doctor::where('spesialis', "Hemodialisa")->orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }
    public function index_igd()
    {
        $doctors = Doctor::where('spesialis', "Instalasi Gawat Darurat")->orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }
    public function index_gigi()
    {
        $doctors = Doctor::where('spesialis', "Klinik Gigi")->orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }
    public function index_kecantikan()
    {
        $doctors = Doctor::where('spesialis', "Klinik Kecantikan & Estetika")->orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }
    public function index_bedah_umum()
    {
        $doctors = Doctor::where('spesialis', "Poli Spesialis Bedah Umum")->orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }
    public function index_anak()
    {
        $doctors = Doctor::where('spesialis', "Spesialis Anak")->orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }
    public function index_jiwa()
    {
        $doctors = Doctor::where('spesialis', "Spesialis Jiwa")->orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }
    public function index_kandungan()
    {
        $doctors = Doctor::where('spesialis', "Spesialis Kandungan")->orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }
    public function index_dalam()
    {
        $doctors = Doctor::where('spesialis', "Spesialis Penyakit Dalam")->orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }
    public function index_saraf()
    {
        $doctors = Doctor::where('spesialis', "Spesialis Saraf")->orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }
    public function index_radiologi()
    {
        $doctors = Doctor::where('spesialis', "Unit Radiologi")->orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }
    public function index_tht()
    {
        $doctors = Doctor::where('spesialis', "Klinik Spesialis THT")->orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }
    public function index_paru()
    {
        $doctors = Doctor::where('spesialis', "Spesialis Paru")->orderBy('name', 'asc')->get();
        $imageDoctors = [];
        foreach ($doctors as $doctor) {
            if (Cloudder::show($doctor->cover_image)) {

                $image = Cloudder::show($doctor->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    } else if ($x == 6) {
                        array_push($array, 'c_scale,h_202,q_100,w_280');
                    }
                }
                $comma_separated = implode("/", $array);
                $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('doctors' => $doctors, 'imageDoctors' => $imageDoctors);

        return view('doctors.index')->with($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('doctors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'spesialis' => 'required',
            'cover_image' => 'nullable|mimes:jpeg,bmp,jpg,png|between:1, 6000',

            'senin_mulai' => 'nullable',
            'senin_selesai' => 'nullable',

            'selasa_mulai' => 'nullable',
            'selasa_selesai' => 'nullable',

            'rabu_mulai' => 'nullable',
            'rabu_selesai' => 'nullable',

            'kamis_mulai' => 'nullable',
            'kamis_selesai' => 'nullable',

            'jumat_mulai' => 'nullable',
            'jumat_selesai' => 'nullable',

            'sabtu_mulai' => 'nullable',
            'sabtu_selesai' => 'nullable',

        ]);

        if ($request->hasFile('cover_image')) {
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $image_name = $request->file('cover_image')->getRealPath();;

            $fileNameToStore = $filename . '_' . time();
            Log::emergency($image_name);


            Cloudder::upload($image_name, $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg' . '_' . time();
            $image_name = Storage::path('img/profile-avatar.png');
            Cloudder::upload($image_name, $fileNameToStore);
            // Cloudder::upload($image_name, $fileNameToStore);
        }
        $doctor = new Doctor;
        $doctor->name = $request->input('name');
        $doctor->spesialis = $request->input('spesialis');
        $doctor->cover_image = $fileNameToStore;


        $doctor->senin_mulai = $request->input('senin_mulai');
        $doctor->senin_selesai = $request->input('senin_selesai');

        $doctor->selasa_mulai = $request->input('selasa_mulai');
        $doctor->selasa_selesai = $request->input('selasa_selesai');

        $doctor->rabu_mulai = $request->input('rabu_mulai');
        $doctor->rabu_selesai = $request->input('rabu_selesai');

        $doctor->kamis_mulai = $request->input('kamis_mulai');
        $doctor->kamis_selesai = $request->input('kamis_selesai');

        $doctor->jumat_mulai = $request->input('jumat_mulai');
        $doctor->jumat_selesai = $request->input('jumat_selesai');

        $doctor->sabtu_mulai = $request->input('sabtu_mulai');
        $doctor->sabtu_selesai = $request->input('sabtu_selesai');
        $doctor->save();

        return redirect('/dokters')->with('success', 'Dokter Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $doctor = Doctor::find($id);
        $imageDoctors = [];
        if (Cloudder::show($doctor->cover_image)) {

            $image = Cloudder::show($doctor->cover_image);

            $var = preg_split("#/#", $image);
            $array = [];
            for ($x = 0; $x < count($var); $x++) {
                if ($x != 6) {
                    array_push($array, $var[$x]);
                }
            }
            $comma_separated = implode("/", $array);
            $imageDoctors[] = ['nama' => $doctor->cover_image, 'image' => $comma_separated];
        }

        $data = array('doctors' => $doctor, 'imageDoctors' => $imageDoctors);

        if ($doctor->senin_mulai) {
            $time_senin_mulai = Carbon::createFromFormat('H:i:s', $doctor->senin_mulai);
            $doctor->senin_mulai = $time_senin_mulai->format('H:i');
        }
        if ($doctor->senin_selesai) {
            $time_senin_selesai = Carbon::createFromFormat('H:i:s', $doctor->senin_selesai);
            $doctor->senin_selesai = $time_senin_selesai->format('H:i');
        }
        if ($doctor->selasa_mulai) {
            $time_selasa_mulai = Carbon::createFromFormat('H:i:s', $doctor->selasa_mulai);
            $doctor->selasa_mulai = $time_selasa_mulai->format('H:i');
        }
        if ($doctor->selasa_selesai) {
            $time_selasa_selesai = Carbon::createFromFormat('H:i:s', $doctor->selasa_selesai);
            $doctor->selasa_selesai = $time_selasa_selesai->format('H:i');
        }
        if ($doctor->rabu_mulai) {
            $time_rabu_mulai = Carbon::createFromFormat('H:i:s', $doctor->rabu_mulai);
            $doctor->rabu_mulai = $time_rabu_mulai->format('H:i');
        }
        if ($doctor->rabu_selesai) {
            $time_rabu_selesai = Carbon::createFromFormat('H:i:s', $doctor->rabu_selesai);
            $doctor->rabu_selesai = $time_rabu_selesai->format('H:i');
        }
        if ($doctor->kamis_mulai) {
            $time_kamis_mulai = Carbon::createFromFormat('H:i:s', $doctor->kamis_mulai);
            $doctor->kamis_mulai = $time_kamis_mulai->format('H:i');
        }
        if ($doctor->kamis_selesai) {
            $time_kamis_selesai = Carbon::createFromFormat('H:i:s', $doctor->kamis_selesai);
            $doctor->kamis_selesai = $time_kamis_selesai->format('H:i');
        }
        if ($doctor->jumat_mulai) {
            $time_jumat_mulai = Carbon::createFromFormat('H:i:s', $doctor->jumat_mulai);
            $doctor->jumat_mulai = $time_jumat_mulai->format('H:i');
        }
        if ($doctor->jumat_selesai) {
            $time_jumat_selesai = Carbon::createFromFormat('H:i:s', $doctor->jumat_selesai);
            $doctor->jumat_selesai = $time_jumat_selesai->format('H:i');
        }
        if ($doctor->sabtu_mulai) {
            $time_sabtu_mulai = Carbon::createFromFormat('H:i:s', $doctor->sabtu_mulai);
            $doctor->sabtu_mulai = $time_sabtu_mulai->format('H:i');
        }
        if ($doctor->sabtu_selesai) {
            $time_sabtu_selesai = Carbon::createFromFormat('H:i:s', $doctor->sabtu_selesai);
            $doctor->sabtu_selesai = $time_sabtu_selesai->format('H:i');
        }
        $data = array('doctor' => $doctor, 'imageDoctors' => $imageDoctors);

        return view('doctors.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $doctor = Doctor::find($id);
        if ($doctor->senin_mulai) {
            $time_senin_mulai = Carbon::createFromFormat('H:i:s', $doctor->senin_mulai);
            $doctor->senin_mulai = $time_senin_mulai->format('H:i');
        }
        if ($doctor->senin_selesai) {
            $time_senin_selesai = Carbon::createFromFormat('H:i:s', $doctor->senin_selesai);
            $doctor->senin_selesai = $time_senin_selesai->format('H:i');
        }
        if ($doctor->selasa_mulai) {
            $time_selasa_mulai = Carbon::createFromFormat('H:i:s', $doctor->selasa_mulai);
            $doctor->selasa_mulai = $time_selasa_mulai->format('H:i');
        }
        if ($doctor->selasa_selesai) {
            $time_selasa_selesai = Carbon::createFromFormat('H:i:s', $doctor->selasa_selesai);
            $doctor->selasa_selesai = $time_selasa_selesai->format('H:i');
        }
        if ($doctor->rabu_mulai) {
            $time_rabu_mulai = Carbon::createFromFormat('H:i:s', $doctor->rabu_mulai);
            $doctor->rabu_mulai = $time_rabu_mulai->format('H:i');
        }
        if ($doctor->rabu_selesai) {
            $time_rabu_selesai = Carbon::createFromFormat('H:i:s', $doctor->rabu_selesai);
            $doctor->rabu_selesai = $time_rabu_selesai->format('H:i');
        }
        if ($doctor->kamis_mulai) {
            $time_kamis_mulai = Carbon::createFromFormat('H:i:s', $doctor->kamis_mulai);
            $doctor->kamis_mulai = $time_kamis_mulai->format('H:i');
        }
        if ($doctor->kamis_selesai) {
            $time_kamis_selesai = Carbon::createFromFormat('H:i:s', $doctor->kamis_selesai);
            $doctor->kamis_selesai = $time_kamis_selesai->format('H:i');
        }
        if ($doctor->jumat_mulai) {
            $time_jumat_mulai = Carbon::createFromFormat('H:i:s', $doctor->jumat_mulai);
            $doctor->jumat_mulai = $time_jumat_mulai->format('H:i');
        }
        if ($doctor->jumat_selesai) {
            $time_jumat_selesai = Carbon::createFromFormat('H:i:s', $doctor->jumat_selesai);
            $doctor->jumat_selesai = $time_jumat_selesai->format('H:i');
        }
        if ($doctor->sabtu_mulai) {
            $time_sabtu_mulai = Carbon::createFromFormat('H:i:s', $doctor->sabtu_mulai);
            $doctor->sabtu_mulai = $time_sabtu_mulai->format('H:i');
        }
        if ($doctor->sabtu_selesai) {
            $time_sabtu_selesai = Carbon::createFromFormat('H:i:s', $doctor->sabtu_selesai);
            $doctor->sabtu_selesai = $time_sabtu_selesai->format('H:i');
        }
        return view('doctors.edit')->with('doctor', $doctor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'spesialis' => 'required',

            'senin_mulai' => 'nullable',
            'senin_selesai' => 'nullable',

            'selasa_mulai' => 'nullable',
            'selasa_selesai' => 'nullable',

            'rabu_mulai' => 'nullable',
            'rabu_selesai' => 'nullable',

            'kamis_mulai' => 'nullable',
            'kamis_selesai' => 'nullable',

            'jumat_mulai' => 'nullable',
            'jumat_selesai' => 'nullable',

            'sabtu_mulai' => 'nullable',
            'sabtu_selesai' => 'nullable',
        ]);

        $doctor = Doctor::find($id);
        $doctor->name = $request->input('name');
        $doctor->spesialis = $request->input('spesialis');

        $doctor->senin_mulai = $request->input('senin_mulai');
        $doctor->senin_selesai = $request->input('senin_selesai');

        $doctor->selasa_mulai = $request->input('selasa_mulai');
        $doctor->selasa_selesai = $request->input('selasa_selesai');

        $doctor->rabu_mulai = $request->input('rabu_mulai');
        $doctor->rabu_selesai = $request->input('rabu_selesai');

        $doctor->kamis_mulai = $request->input('kamis_mulai');
        $doctor->kamis_selesai = $request->input('kamis_selesai');

        $doctor->jumat_mulai = $request->input('jumat_mulai');
        $doctor->jumat_selesai = $request->input('jumat_selesai');

        $doctor->sabtu_mulai = $request->input('sabtu_mulai');
        $doctor->sabtu_selesai = $request->input('sabtu_selesai');
        $doctor->save();

        $doctor->save();
        return redirect('/dokters')->with('success', 'Dokter Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doctor = Doctor::find($id);
        $doctor->delete();
        return redirect('/dokters')->with('success', 'Dokter Dihapus');
    }
}
