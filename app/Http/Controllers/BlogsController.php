<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Blog;

use Carbon\Carbon;

use Illuminate\Support\Facades\Log;

use JD\Cloudder\Facades\Cloudder;

class BlogsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::orderBy('created_at', 'desc')->paginate(10);
        $imageBlogs = [];

        foreach ($blogs as $blog) {
            if (Cloudder::show($blog->cover_image)) {

                $image = Cloudder::show($blog->cover_image);

                $var = preg_split("#/#", $image);
                $array = [];
                for ($x = 0; $x < count($var); $x++) {
                    if ($x != 6) {
                        array_push($array, $var[$x]);
                    }
                }
                $comma_separated = implode("/", $array);
                $imageBlogs[] = ['nama' => $blog->cover_image, 'image' => $comma_separated];
            }
        }
        $data = array('blogs' => $blogs,  'imageBlogs' => $imageBlogs);
        return view('blogs.index')->with($data);
        // return view('blogs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'nullable|mimes:jpeg,bmp,jpg,png|between:1, 6000',
        ]);

        if ($request->hasFile('cover_image')) {
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $image_name = $request->file('cover_image')->getRealPath();;

            $fileNameToStore = $filename . '_' . time();

            Cloudder::upload($image_name, $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        $blog = new Blog;
        $blog->title = $request->input('title');
        $blog->body = $request->input('body');
        $blog->cover_image = $fileNameToStore;
        $blog->save();
        return redirect('/artikel')->with('success', 'Artikel Dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blogs = Blog::find($id);
        Carbon::setLocale('id');
        $blogs->created_at_str = Carbon::parse($blogs->created_at)->locale('id')->isoFormat('dddd, MMMM D YYYY');
        $imageBlogs = [];
        if (Cloudder::show($blogs->cover_image)) {

            $image = Cloudder::show($blogs->cover_image);

            $var = preg_split("#/#", $image);
            $array = [];
            for ($x = 0; $x < count($var); $x++) {
                if ($x != 6) {
                    array_push($array, $var[$x]);
                }
            }
            $comma_separated = implode("/", $array);
            $imageBlogs[] = ['nama' => $blogs->cover_image, 'image' => $comma_separated];
        }
        $data = array('blogs' => $blogs,  'imageBlogs' => $imageBlogs);
        return view('blogs.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);

        return view('blogs.edit')->with('blog', $blog);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
        ]);
        $blogs = Blog::find($id);
        $blogs->title = $request->input('title');
        $blogs->body = $request->input('body');
        $blogs->save();
        return redirect('/artikel')->with('success', 'Artikel Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogs = Blog::find($id);
        $blogs->delete();
        return redirect('/artikel')->with('success', 'Artikel Dihapus');
    }
}
