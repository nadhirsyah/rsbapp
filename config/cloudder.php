<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Cloudinary API configuration
    |--------------------------------------------------------------------------
    |
    | Before using Cloudinary you need to register and get some detail
    | to fill in below, please visit cloudinary.com.
    |
    */
    'cloud_name' => env('CLOUDINARY_CLOUD_NAME', 'dirda1899'),
    'cloudName'  => env('CLOUDINARY_CLOUD_NAME', 'dirda1899'),
    'baseUrl'    => env('CLOUDINARY_BASE_URL', 'http://res.cloudinary.com/' . env('CLOUDINARY_CLOUD_NAME')),
    'secureUrl'  => env('CLOUDINARY_SECURE_URL', 'https://res.cloudinary.com/' . env('CLOUDINARY_CLOUD_NAME')),
    'apiBaseUrl' => env('CLOUDINARY_API_BASE_URL', 'https://api.cloudinary.com/v1_1/' . env('CLOUDINARY_CLOUD_NAME')),
    'apiKey'     => env('CLOUDINARY_API_KEY', '955955543399715'),
    'apiSecret'  => env('CLOUDINARY_API_SECRET', 'JzoxgyRFMHnSjsvv48QyMlPfvOU'),

    'scaling'    => [
        'format' => 'png',
        'width'  => 150,
        'height' => 150,
        'crop'   => 'fit',
        'effect' => null
    ],

];
