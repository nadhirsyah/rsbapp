<?php

use App\Http\Controllers\PagesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', "PagesController@index");
Route::get('/profile', "PagesController@about");

// Route::get('/users/{id}/{name}', function ($id, $name) {
//     return 'This is user ' . $name . ' with an id of ' . $id;
// });

Route::resource('artikel', 'BlogsController');

// Route::resource('blogs', 'BlogsController');

Route::resource('dokters', 'DoctorsController')->only([
    'index', 'create', 'store', 'show', 'edit', 'store', 'destroy', 'update'
]);

Route::resource('layanan', 'ServicesController');

Route::get('/admin', 'HomeController@adminHome')->middleware('is_admin');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/beds', 'BedsController@index');

Route::get('/dokter/hemodialisa', 'DoctorsController@index_hemodialisa');
Route::get('/dokter/igd', 'DoctorsController@index_igd');
Route::get('/dokter/gigi', 'DoctorsController@index_gigi');
Route::get('/dokter/kecantikan', 'DoctorsController@index_kecantikan');
Route::get('/dokter/bedah-umum', 'DoctorsController@index_bedah_umum');
Route::get('/dokter/anak', 'DoctorsController@index_anak');
Route::get('/dokter/jiwa', 'DoctorsController@index_jiwa');
Route::get('/dokter/kandungan', 'DoctorsController@index_kandungan');
Route::get('/dokter/dalam', 'DoctorsController@index_dalam');
Route::get('/dokter/saraf', 'DoctorsController@index_saraf');
Route::get('/dokter/radiologi', 'DoctorsController@index_radiologi');
Route::get('/dokter/tht', 'DoctorsController@index_tht');
Route::get('/dokter/paru', 'DoctorsController@index_paru');

Route::get('/upload', 'ImageUploadController@home');

Route::post('/upload/images', [
    'uses'   =>  'ImageUploadController@uploadImages',
    'as'     =>  'uploadImage'
]);
Route::get('/upload/show', 'ImageUploadController@show');
